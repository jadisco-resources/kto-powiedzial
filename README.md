This is a console prototype of a Blazor Server application, availabe at <https://gitlab.com/bltzkrg22/poorchat-quiz>.

The only thing of interest is the usage of default .NET’s Dependency Injection provider directly inside a console app. Everything else is superseeded by the web application.
