﻿using KtoPowiedzial.Data;
using KtoPowiedzial.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KtoPowiedzial
{
    public class Quiz
    {
        private const int MemorySize = 5;
        private Queue<Question> questions;
        private IQuestionRepository _questionRepository;

        public Quiz(IQuestionRepository questionRepository)
        {
            _questionRepository = questionRepository;
        }


        public void Play()
        {
            ConsoleKeyInfo userInput;
            bool breakCondition = false;
            while (breakCondition == false)
            {
                FillQuestionQueue(MemorySize);
                var question = questions.Dequeue();
                Console.WriteLine($"Czyj to cytat? „{question.QuestionText}”");

                string[] answers = new[] 
                    { question.CorrectUserNickname, question.WrongUserNicknameA, question.WrongUserNicknameB };

                Rng.KnuthShuffle(answers);
                Console.WriteLine($"A. {answers[0]}\tB. {answers[1]}\tC. {answers[2]}");

                string userAnswer = null;
                bool validInputRegistered = false;

                while (!validInputRegistered)
                {
                    userInput = Console.ReadKey();
                    Console.WriteLine();

                    switch (userInput.Key)
                    {
                        case ConsoleKey.X: validInputRegistered = true; breakCondition = true; break;
                        case ConsoleKey.A: validInputRegistered = true; userAnswer = answers[0]; break;
                        case ConsoleKey.B: validInputRegistered = true; userAnswer = answers[1]; break;
                        case ConsoleKey.C: validInputRegistered = true; userAnswer = answers[2]; break;
                        default:
                            userAnswer = null;
                            Console.WriteLine("Wcisnij A, B lub C by wybrać odpowiedź, lub X by zakończyć quiz.");
                            break;
                    }
                }

                if (breakCondition == true)
                {
                    break;
                }

                if (userAnswer == question.CorrectUserNickname)
                {
                    Console.WriteLine("Udzieliłeś poprawnej odpowiedzi.");
                }
                else
                {
                    Console.WriteLine($"Niestety, poprawna odpowiedź to: {question.CorrectUserNickname}.");
                }


                Console.WriteLine("Wciśnij X by zakończyć grę, lub dowolny inny klawisz by kontynuować.");

                userInput = Console.ReadKey();
                Console.WriteLine();

                if (userInput.Key == ConsoleKey.X)
                {
                    breakCondition = true;
                }
            }
        }

        private void FillQuestionQueue(int howMany)
        {
            if (questions is null)
            {
                questions = new Queue<Question>();
            }

            if (questions.Count == 0)
            {
                for (int i = 1; i <= howMany; i++)
                {
                    questions.Enqueue(_questionRepository.GetRandomQuestion());
                }
            }
        }
    }
}
