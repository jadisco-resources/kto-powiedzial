﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KtoPowiedzial
{
    public static class Rng
    {
        private static Random rng = new Random();

        public static int GetRandom() => rng.Next();

        /// <summary>
        /// Generates a random number in range from 0 to max - 1, inclusive.
        /// </summary>
        public static int GetRandom(int max) => rng.Next(max);

        public static void KnuthShuffle<T>(T[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                int j = Rng.rng.Next(i, array.Length); // Don't select from the entire array on subsequent loops
                T temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }

        public static void DictShuffle(Dictionary<string, string> dict)
        {
            int n = dict.Count;

            string[] keyArray = dict.Keys.ToArray();
            KnuthShuffle(keyArray);

            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                string value = dict[keyArray[k]];
                dict[keyArray[k]] = dict[keyArray[n]];
                dict[keyArray[n]] = value;
            }
        }

    }
}
