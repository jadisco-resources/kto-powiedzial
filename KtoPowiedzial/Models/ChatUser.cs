﻿using System.ComponentModel.DataAnnotations;

namespace KtoPowiedzial.Models
{
    public class ChatUser
    {
        [Key]
        public string UserNickname { get; set; }

        [RegularExpression(@"#[0-9]{6}")]
        public string HtmlColor { get; set; }
    }
}
