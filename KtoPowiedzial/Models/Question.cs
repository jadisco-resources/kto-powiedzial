﻿using System.ComponentModel.DataAnnotations;

namespace KtoPowiedzial.Models
{
    public class Question
    {
        [Key]
        public uint Id { get; set; }
        public string QuestionText { get; set; }
        public string CorrectUserNickname { get; set; }
        public string WrongUserNicknameA { get; set; }
        public string WrongUserNicknameB { get; set; }

    }
}
