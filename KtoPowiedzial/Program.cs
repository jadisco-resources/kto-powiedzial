﻿using KtoPowiedzial.Data;
using KtoPowiedzial.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace KtoPowiedzial
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceCollection services = new ServiceCollection();
            services.AddSingleton<MyConfig>();
            services.AddSingleton<IQuestionRepository, CsvQuestionRepository>();
            services.AddSingleton<IUserRepository, CsvUserRepository>();


            ServiceProvider serviceProvider = services.BuildServiceProvider();

            // All this ceremony so that we can read appsettings.json globally :(
            //MyConfig configData = serviceProvider.GetService<MyConfig>();


            //string questionsRelativePath = configData.GetConfiguration.GetValue<string>("CsvQuestionRepositoryPath");
            //string usersRelativePath = configData.GetConfiguration.GetValue<string>("CsvUserRepositoryPath");

            //string questionsFullPath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), questionsRelativePath));
            //string usersFullPath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), usersRelativePath));



            var questionRepository = serviceProvider.GetService<IQuestionRepository>();

            //var randomQuestion = micCheck.GetRandomQuestion();

            //PrintQuestionToConsole(randomQuestion);



            var quiz = new Quiz(questionRepository);
            quiz.Play();



            if (serviceProvider is IDisposable)
            {
                ((IDisposable)serviceProvider).Dispose();
            }

            //_ = Console.ReadKey();
        }

        private static void PrintQuestionToConsole(Question question)
        {
            
        }
    }
}
