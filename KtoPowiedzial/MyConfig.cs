﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace KtoPowiedzial
{
    public class MyConfig
    {
        private readonly ConfigurationBuilder _builder;
        private readonly IConfiguration _config;

        public IConfiguration GetConfiguration => _config;

        public MyConfig()
        {
            _builder = new ConfigurationBuilder();
            BuildConfig(_builder);
            _config = _builder.Build();
        }

        private static void BuildConfig(IConfigurationBuilder builder)
        {
            builder.SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
        }
    }
}
