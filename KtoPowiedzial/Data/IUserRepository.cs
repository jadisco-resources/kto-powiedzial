﻿namespace KtoPowiedzial.Data
{
    public interface IUserRepository
    {
        public string GetUserColor(string nickname);
    }
}
