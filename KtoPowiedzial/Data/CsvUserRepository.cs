﻿using KtoPowiedzial.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;

namespace KtoPowiedzial.Data
{
    public class CsvUserRepository : IUserRepository
    {
        private Dictionary<string, ChatUser> _users;
        public string GetUserColor(string nickname)
        {
            if (_users.ContainsKey(nickname))
            {
                return _users[nickname].HtmlColor;
            }
            else
            {
                throw new ApplicationException($"User with name {nickname} is not defined in the database");
            }
        }

        public CsvUserRepository(MyConfig configData)
        {
            _users = new Dictionary<string, ChatUser>();

            string usersRelativePath = configData.GetConfiguration.GetValue<string>("CsvUserRepositoryPath");
            string usersFullPath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), usersRelativePath));

            StreamReader fileReader = new StreamReader(usersFullPath);
            string csvLine;
            while ((csvLine = fileReader.ReadLine()) is not null)
            {
                string[] fields = csvLine.Split(',');
                string userNickname = fields[0];
                string htmlColor = fields[1];

                //var newUser = new ChatUser() { UserNickname = fields[0], HtmlColor = fields[1] };

                if (_users.ContainsKey(userNickname))
                {
                    throw new ApplicationException($"Multiple definitions in file {usersRelativePath} for user {userNickname}");
                }
                else
                {
                    _users[userNickname] = new ChatUser() { UserNickname = userNickname, HtmlColor = htmlColor };
                }
            }
        }
    }
}
