﻿using KtoPowiedzial.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace KtoPowiedzial.Data
{
    class CsvQuestionRepository : IQuestionRepository
    {
        private Dictionary<uint, Question> _questions;


        public Question GetQuestionById(uint id)
            => _questions[id];

        public Question GetRandomQuestion()
            => _questions.ElementAt(Rng.GetRandom(_questions.Count)).Value;


        public CsvQuestionRepository(MyConfig configData)
        {
            _questions = new Dictionary<uint, Question>();

            string questionsRelativePath = configData.GetConfiguration.GetValue<string>("CsvQuestionRepositoryPath");
            string questionsFullPath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), questionsRelativePath));

            StreamReader fileReader = new StreamReader(questionsFullPath);
            string csvLine;
            while ((csvLine = fileReader.ReadLine()) is not null)
            {
                string[] fields = csvLine.Split(',');
                uint id = UInt32.Parse(fields[0]);
                string questionText = fields[1];
                string correctUserNickname = fields[2];
                string wrongUserNicknameA = fields[3];
                string wrongUserNicknameB = fields[4];

                if (_questions.ContainsKey(id))
                {
                    throw new ApplicationException($"Multiple definitions in file {questionsRelativePath} for question id {id}");
                }
                else
                {
                    _questions[id] = new Question()
                    {
                        Id = id,
                        QuestionText = questionText,
                        CorrectUserNickname = correctUserNickname,
                        WrongUserNicknameA = wrongUserNicknameA,
                        WrongUserNicknameB = wrongUserNicknameB
                    };
                }
            }
        }
    }
}
