﻿using KtoPowiedzial.Models;

namespace KtoPowiedzial.Data
{
    public interface IQuestionRepository
    {
        public Question GetRandomQuestion();
        public Question GetQuestionById(uint id);
    }
}
